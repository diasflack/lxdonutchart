(function(global){
	"use strict";

	var LXDonutChart = global.LXDonutChart || {},
		DrawModule = LXDonutChart.DrawModule || {},
		MathModule = LXDonutChart.MathModule || {};

	/**
	 * Create a single representation of Donut chart
	 *
	 * @class
	 * @this {Donut}
	 * @param {string} id - id of donut chart container
	 * @param {Object[]} [slices] - slices of chart
	 * @param {string} [slices.name] - name of the slice
	 * @param {string} [slices.color] - color of the slice
	 * @param {number} [slices.percent] - percent of the slice
	 * @return {object}
	 */
	function Donut(slices, id) {
		this.options = {
			innerRadius: 75,
			outerRadius: 140,
			wrapper: "#LXDonutChart",
			data_element: ".LXDonutChartData",
			data_trigger: ".row",
			slices: [],
			empty: false,
			shift3d: 10,
			svgNS: "http://www.w3.org/2000/svg",
			events: [],
			text: {
				x: 0,
				y: 0,
				color: "#000",
				font: "Roboto, Helvetica, Arial, sans-serif",
				size: "32px",
				weight: "300"
			}
		};


		this.id = id || "#LXDonutChart";
		this.slices = slices ? this.slicesTest(slices) : {};
		this.drawChart();
	}

	Donut.prototype.drawChart = function() {
		DrawModule.options = this.options;
		DrawModule.init(this.id);
		DrawModule.drawChart(this.slices);
		this.options.slices = DrawModule.options.slices;
	};

	/**
	 * get chart slices
	 *
	 * @function
	 * @see Donut
	 * @return {Object[]}
	 */
	Donut.prototype.setSlices = function(slices) {
		this.slices = this.slicesTest(slices);
	};

	Donut.prototype.changeTextOptions = function(text_options) {
		DrawModule.options = this.options;
		DrawModule.changeTextOptions(text_options);
	};

	Donut.prototype.sliceActivate = function(slice) {
		DrawModule.options = this.options;
		DrawModule.draw3d(slice);
	};

	Donut.prototype.sliceDeactivate = function() {
		DrawModule.options = this.options;
		DrawModule.draw2d();
	};

	Donut.prototype.setData = function(activateEventType, deactivateEventType) {
		var triggers, triggers_length, self = this, activateEventFunc, deactivateEventFunc;

		triggers = document.querySelectorAll(this.options.data_element+" "+this.options.data_trigger);
		triggers_length = triggers.length;

		activateEventFunc = function(e) {
			var event = e || window.event;
			var target = event.target || event.srcElement;
			var data = target.getAttribute("data-slice");

			if (data !== null) {
				self.sliceActivate(data);
			}
		};

		deactivateEventFunc = function() {
			self.sliceDeactivate();
		};

		for (var i = 0; i < triggers_length; i++) {
			if (this.options.events[i]) {
				if (this.options.events[i][activateEventType] && this.options.events[i][deactivateEventType]) continue;
			}
			this.options.events[i] = {};
			this.options.events[i][activateEventType] = activateEventFunc;
			this.options.events[i][deactivateEventType] = deactivateEventFunc;

			triggers[i].addEventListener(activateEventType, activateEventFunc);
			triggers[i].addEventListener(deactivateEventType, deactivateEventFunc);

		}

	};

	/**
	 * Check if slices options passed right
	 * Sum of slices percents equal 100.
	 * If not, it is recalculated
	 *
	 * @private
	 * @function
	 * @return {Boolean}
	 */
	Donut.prototype.slicesTest = function(slices) {
		var slices_sum = 0, i, l;

		for(i = 0, l = slices.length; i < l; i++) {
			slices_sum += slices[i].percent;
		}

		if (slices_sum === 0 || !slices_sum) {
			this.options.empty = true;
		} else if (slices_sum !== 100) {
			for(i = 0, l = slices.length; i < l; i++) {
				slices[i].percent = (slices[i].percent/slices_sum)*100;
			}
		}

		return slices;
	};


	LXDonutChart.Donut = Donut;
	global.LXDonutChart = LXDonutChart;

})(this);

