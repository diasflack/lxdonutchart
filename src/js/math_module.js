(function(global){
	"use strict";

	var LXDonutChart = global.LXDonutChart || {},
		MathModule;

	/**
	 * Mathematics module
	 *
	 * @module
	 * @return {Function}
	 */

	/**
	 * Mathmodule Namespace
	 * @namespace
	 */
	MathModule = {

		/**
		 * Convert percents to degrees
		 *
		 * @param {number} percent
		 * @returns {number|boolean} degrees
		 */
		percentToDegrees: function(percent) {
			if (percent > 100 || percent < 0) {
				console.error("Wrong percents! Must be between 0 and 100!");
				return false;
			}
			return (360*percent)/100;
		},

		/**
		 * Sum all degrees
		 *
		 * @param {number} degree
		 * @param {number} startAngle
		 * @returns {number}
		 */
		getFullDegrees: function(degree, startAngle) {
			return degree + startAngle;
		},

		/**
		 * Get start coordinates
		 *
		 * @param {number} outerRadius
		 * @param {number} startAngle
		 * @returns {object}
		 */
		getStartCoord: function(outerRadius, startAngle){
			return this.polarToCartesian(outerRadius, startAngle);
		},

		/**
		 * Get outer arc coordinates
		 *
		 * @param {number} outerRadius
		 * @param {number} fullDegrees
		 * @returns {object}
		 */
		getOuterArcCoord: function(outerRadius, fullDegrees){
			return this.polarToCartesian(outerRadius, fullDegrees);
		},

		/**
		 *  Get inner arc coordinates
		 *
		 * @param {number} innerRadius
		 * @param {number} startAngle
		 * @returns {object}
		 */
		getInnerArcCoord: function(innerRadius, startAngle){
			return this.polarToCartesian(innerRadius, startAngle);
		},

		/**
		 * Get line intersection coordinates
		 *
		 * @param {number} x
		 * @param {number} y
		 * @param {number} innerRadius
		 * @returns {object}
		 */
		getLineCoord: function(x, y, innerRadius){
			return this.lineCircleIntersection(x, y, innerRadius);
		},


		/**
		 * Gets coordinates correction for more smooth paths
		 * @param degree
		 * @returns {object}
		 */
		getCoordinatesCorrection: function(degree) {
			var correction = {}, correctionValue = 0.2;

			switch(true) {
				case (degree > 0 && degree < 90):
					correction.x = -correctionValue;
					correction.y = correctionValue;
					break;
				case (degree >= 90 && degree < 180):
					correction.x = -correctionValue;
					correction.y = -correctionValue;
					break;
				case (degree >= 180 && degree < 270):
					correction.x = correctionValue;
					correction.y = -correctionValue;
					break;
				case (degree >=270 && degree < 359.99):
					correction.x = correctionValue;
					correction.y = correctionValue;
					break;
				default:
					correction.x = 0;
					correction.y = 0;
			}
			return correction;
		},

		/**
		 * Get flags for arcs
		 *
		 * @param {number} degree
		 * @returns {object}
		 */
		getArcFlags: function(degree){
			var arcflags = {};

			degree = degree > 360 ? degree % 360 : degree;

			if (degree >=0 && degree < 180) {
				arcflags.arc1 = "0 0 1";
				arcflags.arc2 = "0 0 0";
			} else if (degree >=180 && degree <= 360) {
				arcflags.arc1 = "0 1 1";
				arcflags.arc2 = "0 1 0";
			}

			return arcflags;
		},

		/** @function Helper function - converts polar coordinates to cartesian */
		polarToCartesian: function(radius, angleInDegrees) {
			var angleInRadians, x, y;

			angleInRadians = angleInDegrees * Math.PI / 180.0;
			x = radius * Math.cos(angleInRadians);
			y = radius * Math.sin(angleInRadians);

			return {
				x: x,
				y: y
			};
		},

		/** @function Helper function - getting coordinates of circle and line intersection **/
		lineCircleIntersection: function(x0,y0,circleRadius) {
			var k, x, y, negative = 1;

			if (x0 < 0) {
				negative = -1;
			}

			k = y0 / x0;
			x = negative*Math.sqrt(circleRadius*circleRadius/(1+k*k));
			y = x*k;

			return {
				x: x,
				y: y
			};
		}
	};

	LXDonutChart.MathModule = MathModule;

	global.LXDonutChart = LXDonutChart;

})(this);

