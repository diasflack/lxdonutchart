(function(global){
	"use strict";

	var LXDonutChart = global.LXDonutChart || {},
		MathModule = LXDonutChart.MathModule,
		DrawModule;

	/**
	 * @module LXDonutCharts Draw module
	 */

	/**
	 * Draw module Namespace
	 * @namespace
	 */
	DrawModule = {
		options: {
			innerRadius: 75,
			outerRadius: 140,
			wrapper: "#LXDonutChart",
			slices: [],
			shift3d: 10,
			empty: false,
			svgNS: "http://www.w3.org/2000/svg",
			text: {
				x: 0,
				y: 0,
				color: "#000",
				font: "Roboto, Helvetica, Arial, sans-serif",
				size: "36px",
				weight: "300"
			}
		},

		init: function(wrapper) {
			this.setWrapper(wrapper);
		},

		/** @function Set wrapper's id */
		setWrapper: function(wrapper) {
			this.options.wrapper = this.checkWrapper(wrapper) || this.options.wrapper;
		},

		/**
		 * Check if wrapper exist and is SVG tag. If not create some.
		 *
		 * @param wrapper
		 * @returns {boolean|string}
		 */
		checkWrapper: function(wrapper) {
			var el, tag, defaultSize, g, size;

			wrapper = wrapper || "#LXDonutChart";

			size = this.options.outerRadius*2 + 2*this.options.shift3d;

			defaultSize = {
				width: size,
				height: size
			};

			el = document.querySelector(wrapper);

			/** check if wrapper element exist */
			if (el === null) {
				el = document.createElementNS(this.options.svgNS,"svg");

				el.setAttribute("id", wrapper.replace(/^#/g, ''));
				setAttributes(el,defaultSize);

				document.body.appendChild(el);
			}

			/** check if sizes right */

			if (el.outerHeight !== size && el.outerWidth !== size) {
				setAttributes(el,defaultSize);
			}

			g = el.getElementsByTagNameNS(this.options.svgNS,"g");

			/** check if <g> element is created */
			if (g.length === 0) {
				g = document.createElementNS(this.options.svgNS,"g");

				/** change coordinates system to center and invert */
				g.setAttribute("class", "LXChartMainGroup");
				g.setAttribute("transform", "translate("+(size/2)+" "+(size/2)+") scale(1,-1)");
				el.appendChild(g);
			}

			tag = el.tagName;

			/** check if wrappers tag is svg */
			if (tag != "SVG" && tag != "svg" ){
				console.error("Wrapper must be created and be 'svg' tag");
				return false;
			}

			return wrapper;
		},

		/**
		 *
		 * Function for changing outer radius
		 *
		 * @param {number} outerRadius
		 */
		setOuterRadius: function(outerRadius) {
			var el, newSize, g;

			el = document.querySelector(this.options.wrapper);

			this.options.outerRadius = outerRadius;

			newSize = {
				width: outerRadius*2,
				height: outerRadius*2
			};

			setAttributes(el,newSize);

			g = el.querySelector(".LXChartMainGroup");
			g.setAttribute("transform", "translate("+outerRadius+" "+outerRadius+") scale(1,-1)");

		},

		/**
		 * Create path with given attributes
		 * @function
		 * @param attributes
		 */
		createPath: function(attributes){
			var path, doc, wrapperClass;

			doc = document;
			wrapperClass = arguments[1] || "LXChartMainGroup";

			path = doc.createElementNS(this.options.svgNS,"path");
			setAttributes(path, attributes);

			doc.querySelector(this.options.wrapper+" ."+wrapperClass).appendChild(path);

		},

		/**
		 * Create group by class
		 * @param groupClass
		 */
		createGroup: function(groupClass) {
			var g;

			g = document.querySelector("."+groupClass);

			if (g === null) {

				g = document.createElementNS(this.options.svgNS, "g");
				g.setAttribute("class", groupClass);

				document.querySelector(this.options.wrapper+" .LXChartMainGroup").appendChild(g);
			}
		},

		/**
		 * Check and save paths in option
		 * @function
		 * @param {object} sliceData
		 * @param {number} startAngle
		 */
		saveSlice: function(sliceData, coordinates, startAngle) {
			var pathNumber = this.options.slices.length,
				slice;

			slice = {
				id: sliceData.name || "slice_" + pathNumber,
				percent: sliceData.percent,
				color: sliceData.color,
				coordinates: coordinates,
				startAngle: startAngle
			};

			this.options.slices.push(slice);
		},

		/**
		 * Get existing slice from option by its id
		 * @function
		 * @param id
		 * @returns {HTMLElement}
		 */
		getSliceByID: function(id){
			var slice;
			slice = getArrayObjectByAttribute(this.options.slices,"id", id);
			return slice;
		},

		/**
		 * Get existing path element from DOM by it's id
		 * @function
		 * @param id
		 * @returns {HTMLElement}
		 */
		getPathElByID: function(id){
			var path = document.getElementById(id);
			return path;
		},

		/**
		 * Set path color
		 * @function
		 * @param id
		 * @param color
		 */
		setPathColor: function(id, color) {
			var path;

			path = this.getPathElByID(id);
			path.setAttribute("fill", color);
		},

		/**
		 * Shade slice color
		 * @param slice
		 * @param shade
		 * @returns {*}
		 */
		shadeSliceColor: function(slice, shade) {
			var shadedColor;
			shadedColor = shadeColor(slice.color, shade);

			this.setPathColor(slice.id, shadedColor);

			return shadedColor;
		},

		/**
		 * Convert all slices to greyscale
		 * @function
		 */
		shadeGreySlices: function () {
			var slices = this.options.slices,
				slicesLength = slices.length;

			for (var i = 0; i < slicesLength; i++) {
				this.setPathColor(slices[i].id, greyColor(slices[i].color));
			}
		},

		/**
		 * Restore slices color to normal
		 * @function
		 */
		restoreSlicesColor: function(){
			var slices = this.options.slices,
				slicesLength = slices.length;

			for (var i = 0; i < slicesLength; i++) {
				this.setPathColor(slices[i].id, slices[i].color);
			}

		},

		/**
		 *  Create's single slice
		 *
		 * @param {object} sliceData
		 * @param {number} startAngle
		 *
		 */
		createSlice: function(sliceData, startAngle) {
			var pathAttrs, sliceCoord;

			sliceCoord = this.getSliceCoordinates(sliceData.percent, startAngle);

			pathAttrs = {
				id: sliceData.name,
				d: this.getSliceDrawAttribute(sliceCoord),
				fill: sliceData.color
			};

			this.createPath(pathAttrs);

			/** save slice data into array */
			this.saveSlice(sliceData, sliceCoord, startAngle);
		},

		/**
		 * Create top slice for 3d slice
		 * @param sliceCoord
		 * @param color
		 */
		createTopSlice: function(sliceCoord, color) {
			var topSliceAttrs;

			this.createGradient(color);

			topSliceAttrs = {
				id: "slice_top",
				d: this.getTopSliceDrawAttribute(sliceCoord),
				fill: "url(#topSliceColor)"
			};

			this.createPath(topSliceAttrs, "group3d");

		},

		/**
		 * Gradient creator function
		 * @param color
		 */
		createGradient: function(color) {
			var gradient, shadedColor, stop1, stop2;

			shadedColor = shadeColor(color,0.5);

			gradient = document.createElementNS(this.options.svgNS,"linearGradient");
			stop1 = document.createElementNS(this.options.svgNS,"stop");
			stop2 = document.createElementNS(this.options.svgNS,"stop");

			setAttributes(gradient, {
				id: "topSliceColor"
			});
			setAttributes(stop1, {
				"stop-color": color,
				offset: 0
			});

			setAttributes(stop2, {
				"stop-color": shadedColor,
				offset: 1
			});

			gradient.appendChild(stop1);
			gradient.appendChild(stop2);

			document.querySelector(this.options.wrapper+" .group3d").appendChild(gradient);
		},

		/**
		 * Create sides for 3d slice
		 * @param sliceCoord
		 * @param shadedColor
		 */
		createSides: function(sliceCoord, shadedColor) {
			var sideLeftAttrs,
				sideRightAttrs;

			sideLeftAttrs = {
				id: "side_left",
				d: this.getSidesDrawAttribute(sliceCoord).left,
				fill: shadedColor
			};

			sideRightAttrs = {
				id: "side_right",
				d: this.getSidesDrawAttribute(sliceCoord).right,
				fill: shadedColor
			};

			this.createPath(sideLeftAttrs, "group3d");
			this.createPath(sideRightAttrs, "group3d");
		},

		/**
		 * Create 3d slice
		 * @function
		 * @param slice
		 */
		create3dSlice: function(slice) {
			var sliceCoord,
				shadedColor;

			sliceCoord = slice.coordinates;
			shadedColor = this.shadeSliceColor(slice, -0.3);

			this.createGroup("group3d");

			this.createSides(sliceCoord, shadedColor);
			this.createTopSlice(sliceCoord, slice.color);

		},

		/**
		 * Create text node
		 * @returns {HTMLElement}
		 */
		createTextNode: function(){
			var text_group, text_node, text_attrs;

			text_group = document.querySelector(this.options.wrapper+" .LXChartTextNode");

			if (text_group === null) {

				text_group = document.createElementNS(this.options.svgNS, "g");
				text_group.setAttribute("class", "LXChartTextNode");
				text_group.setAttribute("transform", "scale(1,-1)");

				text_attrs = {
					x: this.options.text.x,
					y: this.options.text.y,
					fill: this.options.text.color,
					style: "font:"+this.options.text.weight+" "+this.options.text.size+" "+this.options.text.font+";"
				};

				text_node = document.createElementNS(this.options.svgNS, "text");

				setAttributes(text_node, text_attrs);

				text_group.appendChild(text_node);

				document.querySelector(this.options.wrapper+" .LXChartMainGroup").appendChild(text_group);

				return text_group;
			}
		},

		/**
		 * Change text attributes, like position, fill color and font
		 * @param {object} text_attrs
		 */
		changeTextOptions: function(text_attrs) {
			var text_node;

			text_node = document.querySelector(this.options.wrapper+" .LXChartTextNode text");

			setAttributes(text_node, text_attrs);
		},

		/**
		 * Change text in ventral part of chart
		 * @param text
		 */
		changeText: function(text){
			var text_group, text_node,text_coord, text_attrs = {};

			text_group = document.querySelector(this.options.wrapper+" .LXChartTextNode");

			if (text_group === undefined) {
				text_group = this.createTextNode();
			}

			text_node = text_group.getElementsByTagNameNS(this.options.svgNS,"text")[0];

			text_node.textContent = text;

			text_coord = this.getTextCoordinates(text_node);

			text_attrs.x = text_coord.x;
			text_attrs.y = text_coord.y;

			setAttributes(text_node, text_attrs);
		},

		/**
		 * Changing percents
		 * @function
		 * @param percent
		 */
		changePercent: function(percent) {
			var text;

			text = percent.toFixed(3).slice( 0, -1 )+"%";
			this.changeText(text);
		},

		/**
		 * Delete 3d slice
		 * @function
		 */
		delete3dSlice: function(){
			var group3d;

			group3d = document.querySelector(this.options.wrapper+" .group3d");

			if (group3d !== null) {
				group3d.parentNode.removeChild(group3d);
			}
		},

		/**
		 * Fully delete chart
		 * @function
		 */
		deleteChart: function(){
			var mainGroup = document.querySelector(this.options.wrapper+" .LXChartMainGroup");

			while (mainGroup.firstChild) {
				mainGroup.removeChild(mainGroup.firstChild);
			}
			this.options.slices = [];
		},

		/**
		 * Draw entire chart
		 * @param {object} slices
		 */
		drawChart: function(slices) {
			var startAngle = 0,
				numberOfSlices = slices ? slices.length : 0,
				i,
				slice;

			this.deleteChart();

			this.createTextNode();

			if (this.options.empty) {

				this.drawEmptyChart();

			} else {

				for (i = 0; i < numberOfSlices; i++) {
					slice = slices[i];

					this.createSlice({
						name: slice.name,
						percent: slice.percent,
						color: slice.color
					}, startAngle);

					startAngle += MathModule.percentToDegrees(slice.percent);

				}
			}
		},

		/**
		 * @function Creates empty chart
		 */
		drawEmptyChart: function() {
			var emptySlice;

			emptySlice = {
				name: "LXEmptySlice",
				percent: 100,
				color: "#383939"
			};

			this.createSlice(emptySlice, 0);
		},

		/**
		 * Draw 3d slice
		 * @function
		 * @param slice_id
		 */
		draw3d: function(slice_id) {
			var slice;

			slice = this.getSliceByID(slice_id);

			if (slice) {
				this.changePercent(slice.percent);
				this.delete3dSlice();
				this.shadeGreySlices();

				if (slice.percent !== 0) {
					this.create3dSlice(slice);
				}
			}
		},

		/**
		 * Return to 2d
		 * @function
		 */
		draw2d: function() {
			this.delete3dSlice();
			this.changeText("");
			this.restoreSlicesColor();
		},

		/**
		 * Get coordinates of slice
		 * Uses Math module for calculations.
		 *
		 * @see MathModule
		 *
		 * @param {number} percent
		 * @param {number} startAngle
		 * @returns {object}
		 */
		getSliceCoordinates: function(percent, startAngle) {
			var fullDegrees,
				degree,
				outerRadius,
				innerRadius,
				startCoord,
				outerArcCoord,
				innerArcCoord,
				arcflags,
				lineCoord,
				sliceCoord,
				correction;


			percent = this.checkPercent(percent);

			if (percent === 0) {
				return { start_x: 0, start_y: 0, R_x: 0, R_y: 0, arc1_flags: "0 0 0", arc1_end_x: 0, arc1_end_y: 0, L_x: 0, L_y: 0,
					r_x: 0, r_y: 0, arc2_flags: "0 0 0", arc2_end_x: 0, arc2_end_y: 0 };
			}

			startAngle = startAngle || 0;

			outerRadius = this.options.outerRadius;
			innerRadius = this.options.innerRadius;

			degree = MathModule.percentToDegrees(percent);

			fullDegrees = MathModule.getFullDegrees(degree, startAngle);
			correction = MathModule.getCoordinatesCorrection(fullDegrees);

			startCoord = MathModule.getStartCoord(outerRadius, startAngle);
			outerArcCoord = MathModule.getOuterArcCoord(outerRadius, fullDegrees);
			innerArcCoord = MathModule.getInnerArcCoord(innerRadius, startAngle);
			arcflags = MathModule.getArcFlags(degree);

			lineCoord = MathModule.getLineCoord(outerArcCoord.x, outerArcCoord.y, innerRadius);

			sliceCoord = {
				start_x: startCoord.x,
				start_y: startCoord.y,
				R_x: outerRadius,
				R_y: outerRadius,
				arc1_flags: arcflags.arc1,
				arc1_end_x: outerArcCoord.x + correction.x,
				arc1_end_y: outerArcCoord.y + correction.y,
				L_x: lineCoord.x + correction.x,
				L_y: lineCoord.y + correction.y,
				r_x: innerRadius,
				r_y: innerRadius,
				arc2_flags: arcflags.arc2,
				arc2_end_x: innerArcCoord.x,
				arc2_end_y: innerArcCoord.y
			};

			return sliceCoord;

		},

		/**
		 * Get coordinates for top slice of 3d slice
		 * @param sliceCoord
		 * @returns {object}
		 */
		getTopSliceCoordinates: function(sliceCoord) {
			var shift3d = this.options.shift3d,
				topSliceCoord;

			topSliceCoord = {
				start_x: sliceCoord.start_x,
				start_y: sliceCoord.start_y + shift3d,
				R_x: sliceCoord.R_x,
				R_y: sliceCoord.R_y,
				arc1_flags: sliceCoord.arc1_flags,
				arc1_end_x: sliceCoord.arc1_end_x,
				arc1_end_y: sliceCoord.arc1_end_y + shift3d,
				L_x: sliceCoord.L_x,
				L_y: sliceCoord.L_y + shift3d,
				r_x: sliceCoord.r_x,
				r_y: sliceCoord.r_y,
				arc2_flags: sliceCoord.arc2_flags,
				arc2_end_x: sliceCoord.arc2_end_x,
				arc2_end_y: sliceCoord.arc2_end_y + shift3d
			};

			return topSliceCoord;
		},


		/**
		 * Get coordinates of sides for 3d slice
		 * @param coordinates
		 * @returns {object}
		 */
		getSidesCoordinates: function(coordinates) {
			var shift3d = this.options.shift3d,
				sideLeftCoord,
				sideRightCoord;

			sideLeftCoord = {
				start_x: coordinates.start_x,
				start_y: coordinates.start_y,
				line1_x: coordinates.start_x,
				line1_y: coordinates.start_y + shift3d,
				line2_x: coordinates.arc2_end_x,
				line2_y: coordinates.arc2_end_y + shift3d,
				line3_x: coordinates.arc2_end_x,
				line3_y: coordinates.arc2_end_y
			};

			sideRightCoord = {
				start_x: coordinates.arc1_end_x,
				start_y: coordinates.arc1_end_y,
				line1_x: coordinates.arc1_end_x,
				line1_y: coordinates.arc1_end_y + shift3d,
				line2_x: coordinates.L_x,
				line2_y: coordinates.L_y + shift3d,
				line3_x: coordinates.L_x,
				line3_y: coordinates.L_y
			};

			return {
				left: sideLeftCoord,
				right: sideRightCoord
			};

		},

		/**
		 * Get coordinates of text node
		 * @param text_node
		 * @returns {{x: number, y: number}}
		 */
		getTextCoordinates: function(text_node) {
			var text_width, text_height, coords;

			coords = text_node.getBoundingClientRect();

			text_width = coords.width;
			text_height = coords.height;

			return {
				x: -text_width/2,
				y: text_height/4
			};

		},

		/**
		 * Parsing coordinates to d attribute for path svg element
		 * @param {object} sliceCoord
		 * @returns {string}
		 */
		getSliceDrawAttribute: function(sliceCoord) {
			var template;

			template = "M <@start_x> <@start_y> A <@R_x> <@R_y> <@arc1_flags> <@arc1_end_x> <@arc1_end_y> L <@L_x> <@L_y> A <@r_x> <@r_y> <@arc2_flags> <@arc2_end_x> <@arc2_end_y> Z";

			return parseTemplate(template, sliceCoord);

		},

		/**
		 * Get d attribute for top slice of 3d slice
		 * @param sliceCoord
		 * @returns {string}
		 */
		getTopSliceDrawAttribute: function(sliceCoord) {
			var topSliceCoord;

			topSliceCoord = this.getTopSliceCoordinates(sliceCoord);

			return this.getSliceDrawAttribute(topSliceCoord);
		},


		/**
		 * Get d attribute for sides of 3d slice
		 * @param sliceCoord
		 * @returns {{left: *, right: *}}
		 */
		getSidesDrawAttribute: function(sliceCoord) {
			var sidesCoord,
				sideLeftAttr,
				sideRightAttr,
				template;

			sidesCoord = this.getSidesCoordinates(sliceCoord);

			template = "M <@start_x> <@start_y> L <@line1_x> <@line1_y> L <@line2_x> <@line2_y> L <@line3_x> <@line3_y> Z";

			sideLeftAttr =  parseTemplate(template, sidesCoord.left);
			sideRightAttr =  parseTemplate(template, sidesCoord.right);

			return {
				left: sideLeftAttr,
				right: sideRightAttr
			};

		},

		/**
		 * Check if percent equal to zero. If so minimazing it
		 * @param percent
		 * @returns {number}
		 */

		checkPercent: function(percent){
			return percent === 100 ? 99.999 : percent;
		}
	};

	/** @function Helper function for setting multiple attributes to Element @function */
	function setAttributes(el, attrs) {
		if (attrs) {
			for (var key in attrs) {
				el.setAttribute(key, attrs[key]);
			}
		}
	}

	/** @function Helper function for html templates */
	function parseTemplate(tpl, data) {
		var match;
		var re = /<@([^>]+)?>/;
		while(match = re.exec(tpl)) {
			tpl = tpl.replace(match[0], data[match[1]]);
		}
		return tpl;
	}

	/** @function Helper function - getter of object in array */
	function getArrayObjectByAttribute(array, attr, value) {
		var array_length;

		array_length = array.length;

		for (var i = 0; i < array_length; i++) {
			if (array[i].hasOwnProperty(attr)){
				if (array[i][attr] === value) {
					return array[i];
				}
			}

		}

		return false;

	}

	/** @function Helper - shade color from hex */
	function shadeColor(color, percent) {
		var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
		return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
	}

	/** @function Helper - grey color from hex */
	function greyColor(color) {
		var f=parseInt(color.slice(1),16),R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF,p1=0.299,p2=0.587,p3=0.114,x=p1*R+p2*G+p3*B;
		return "#"+(0x1000000+(Math.round(x))*0x10000+(Math.round(x))*0x100+(Math.round(x))).toString(16).slice(1);
	}


	LXDonutChart.DrawModule = DrawModule;
	global.LXDonutChart = LXDonutChart;

})(this);

