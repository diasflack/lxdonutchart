var LXDonutChart = window.LXDonutChart;

function CreateElement(id,tag){
	tag = tag || "svg";
	var el = document.createElement(tag);
	el.setAttribute("id",id);
	document.body.appendChild(el);
	return el;
}

function slicesSum(obj) {
	var sum = _.reduce(obj.slices, function(memo, slice){ return memo + slice.percent; }, 0);
	return sum;
}

describe("LXDonutChartSpecs", function() {

	describe("Basic Specs", function() {
		it("nameSpace is defined", function() {
			expect(LXDonutChart).toBeDefined();
		});
	});

	describe("Donut initialization", function() {
		var donut, id = "LXDonutChart", el, slices;

		beforeEach(function () {
			el = CreateElement(id);
			donut = new LXDonutChart.Donut();
		});

		it("donut created and defined", function () {
			expect(donut).toBeDefined();
		});

		it("Wrapper element saved and attached to body", function () {
			expect(document.querySelector(donut.id)).not.toBeNull();
		});

	});

	describe("Slices tests", function(){
		var donut, id = "LXDonutChart", el, slices;

		beforeEach(function () {
			el = CreateElement(id);
			slices = [
				{
					"name": "apple",
					"color": "#ABCDD00",
					"percent": 12
				},
				{
					"name": "keeper",
					"color": "#CEDD10",
					"percent": 88
				}
			];
			donut = new LXDonutChart.Donut();
			donut.setSlices(slices);
		});

		it("Slices is settable", function() {
			expect(donut.slices).toBeDefined();
		});

		it("Slices sum is equal 100 percent", function() {
			expect(slicesSum(donut)).toEqual(100);
		});

		/*

		it("Slice color option is settable by name and unique", function() {
			var slice;
			slice = donut.getSliceByName("apple");

			slice.color = "#ADCDEF";

			expect(donut.slices[0].color).toEqual("#ADCDEF");

			slice.color = "#CEDD10";
			expect(donut.slices[0].color).toEqual("#ADCDEF");
		});

		it("Slice percent options is settable by name and sum rearranged", function() {
			var slice;
			slice = donut.getSliceByName("keeper");

			slice.percent = 16;

			expect(donut.slices[1].percent).toEqual(16);
			expect(slicesSum(donut)).toEqual(100);
		});

		it("Slice is destroyable by name and percent sum rearranged", function() {
			var slice;

			slice = donut.destroySliceByName("apple");

			expect(donut.slices).not.toContain(slice);
			expect(slicesSum(donut)).toEqual(100);
		});

		*/

	});

	describe("Draw module tests", function() {
		var DrawModule = LXDonutChart.DrawModule,
			MathModule = LXDonutChart.MathModule,
			svg,
			slices;

		svg = CreateElement("svg");
		slices = [
			{
				"name": "apple",
				"color": "#ABCDD00",
				"percent": 12
			},
			{
				"name": "keeper",
				"color": "#CEDD10",
				"percent": 88
			}
		];

		it("Draw wrapper is set and default equal LXDonutChart", function() {
			expect(DrawModule.options.wrapper).toBe("#LXDonutChart");
			DrawModule.setWrapper("svg");
			expect(DrawModule.options.wrapper).toBe("svg");
		});

		it("Paths created with given attributes", function() {
			var attributes = {id: "slice_0", d:"M 100 350 l 150 -300"};

			DrawModule.createPath(attributes);
			expect(document.getElementsByTagName("path")[0].getAttribute("d")).toBe(attributes.d);

		});

		it("Group created", function() {
			var groupClass = "group3d";

			DrawModule.createGroup(groupClass);
			expect(document.getElementsByClassName(groupClass)[0]).not.toBeNull();

		});

		it("Set path color by ID", function() {
			var pathID = "slice_0",
				color = "#ABB25F";

			DrawModule.setPathColor(pathID, color);

			expect(document.getElementById("slice_0").getAttribute("fill")).toBe(color);

		});

		it("Draw attribute parsed and created", function() {
			var percent, startAngle, sliceCoord;

			percent = 50;
			startAngle = 0;
			sliceCoord = DrawModule.getSliceCoordinates(percent,startAngle);
			expect(DrawModule.getSliceDrawAttribute(sliceCoord)).toEqual("M 140 0 A 140 140 0 1 1 -139.8 -0.19999999999998286 L -74.8 -0.19999999999999082 A 75 75 0 1 0 75 0 Z");

			percent = 70;
			startAngle = 40;
			sliceCoord = DrawModule.getSliceCoordinates(percent,startAngle);
			expect(DrawModule.getSliceDrawAttribute(sliceCoord)).toEqual("M 107.24622203665692 89.9902653561155 A 140 140 0 1 1 52.64492307822768 -129.60573963935025 L 28.295494506193396 -69.33878909250906 A 75 75 0 1 0 57.45333323392335 48.209070726490445 Z");

		});

		it("Create slice with given attributes(percent,color,name)", function() {
			var sliceAttrs = {};

			sliceAttrs.name = "apple";
			sliceAttrs.percent = 12;
			sliceAttrs.color = "#AB25BB";

			MathModule = LXDonutChart.MathModule;

			DrawModule.createSlice(sliceAttrs);
			expect(document.getElementById("apple").getAttribute("fill")).toBe(sliceAttrs.color);

		});

		it("Slice saved in options and gettable", function() {
			var slice, slice_id = "apple";

			slice = DrawModule.getSliceByID(slice_id);
			expect(slice.id).toBe("apple");
			expect(slice.percent).toBe(12);
			expect(slice.color).toBe("#AB25BB");
		});

		it("Shade slice color", function() {
			var slice_id = "apple",
				slice,
				shade = 0.3;

			slice = DrawModule.getSliceByID(slice_id);
			DrawModule.shadeSliceColor(slice, shade);

			expect(document.getElementById(slice_id).getAttribute("fill")).toBe("#c466cf");

		});

		it("Grey slices color", function() {
			var slice_id = "apple";

			DrawModule.shadeGreySlices();

			expect(document.getElementById(slice_id).getAttribute("fill")).toBe("#5e5e5e");

		});

		it("Return slices color to normal", function() {
			var slice_id = "apple", slice;

			slice = DrawModule.getSliceByID(slice_id);
			DrawModule.restoreSlicesColor();

			expect(document.getElementById(slice.id).getAttribute("fill")).toBe(slice.color);

		});

		it("DrawChart get slices and use it", function() {
			DrawModule.drawChart(slices);
			expect(document.getElementById("apple")).not.toBeNull();
			expect(document.getElementById("keeper")).not.toBeNull();
		});

		describe("3D translation", function() {
			var slice_id, slice;

			beforeEach(function(){
				slice_id = "apple";
				slice = DrawModule.getSliceByID(slice_id);
				DrawModule.createGroup("group3d");
			});

			it("Draw additional top slice for 3d", function(){
				DrawModule.createTopSlice(slice.coordinates, slice.color);
				expect(document.getElementById("slice_top")).not.toBeNull();
			});

			it("Draw additional sides for 3d", function(){
				DrawModule.createSides(slice.coordinates, slice.color);
				expect(document.getElementById("side_left")).not.toBeNull();
				expect(document.getElementById("side_right")).not.toBeNull();
			});

			it("Create 3D slice", function(){
				DrawModule.create3dSlice(slice);
				expect(document.getElementById("slice_top")).not.toBeNull();
				expect(document.getElementById("side_left")).not.toBeNull();
				expect(document.getElementById("side_right")).not.toBeNull();
			});

			it("Delete 3D slice", function(){
				DrawModule.delete3dSlice();
				expect(document.getElementById("side_left")).toBeNull();
				expect(document.getElementById("side_right")).toBeNull();
				expect(document.getElementById("slice_top")).toBeNull();
			});

			it("Draw 3D slice", function(){
				DrawModule.draw3d(slice_id);
				expect(document.getElementById(slice_id)).not.toBeNull();
			});

			it("Return to normal", function(){
				DrawModule.draw2d();
				expect(document.getElementById("side_left")).toBeNull();
				expect(document.getElementById("side_right")).toBeNull();
				expect(document.getElementById("slice_top")).toBeNull();
			});

			afterEach(function(){
				DrawModule.delete3dSlice();
			});

		});

		describe("Text nodes", function() {

			it("Create text node", function(){
				DrawModule.createTextNode();
				expect(document.getElementById(DrawModule.options.wrapper).getElementsByClassName("textGroup")[0]).not.toBeNull();
				expect(document.getElementById(DrawModule.options.wrapper).getElementsByTagName("text")[0]).not.toBeNull();
			});

			it("Change text in text node", function(){
				var text = "50.00%";

				DrawModule.changeText(text);
				expect(document.querySelector(DrawModule.options.wrapper+" text").textContent).toBe(text);
			});

		});

	});

	describe("Math module tests", function() {
		var MathModule = LXDonutChart.MathModule;

		it("Math module is set and active", function() {
			expect(MathModule).toBeDefined();
		});

		it("Degrees converting right", function() {
			var percent;

			percent = 100;
			expect(MathModule.percentToDegrees(percent)).toEqual(360);

			percent = 10;
			expect(MathModule.percentToDegrees(percent)).toEqual(36);

			percent = 50;
			expect(MathModule.percentToDegrees(percent)).toEqual(180);
		});

		it("Get degrees + starting angle", function() {
			var startingAngle, degree;

			degree = 460;
			startingAngle = 60;
			expect(MathModule.getFullDegrees(degree,startingAngle)).toEqual(520);

			degree = 700;
			startingAngle = 80;
			expect(MathModule.getFullDegrees(degree,startingAngle)).toEqual(780);

			degree = 320;
			startingAngle = 0;
			expect(MathModule.getFullDegrees(degree,startingAngle)).toEqual(320);
		});

		it("Get start coordinates", function() {
			var startAngle, outerRadius;

			startAngle = 0;
			outerRadius = 140;
			expect(MathModule.getStartCoord(outerRadius,startAngle)).toEqual({x:140,y:0});

			startAngle = 90;
			outerRadius = 60;
			expect(MathModule.getStartCoord(outerRadius,startAngle).x).toBeCloseTo(0);
			expect(MathModule.getStartCoord(outerRadius,startAngle).y).toBeCloseTo(60);

			startAngle = 180;
			outerRadius = 10;
			expect(MathModule.getStartCoord(outerRadius,startAngle).x).toBeCloseTo(-10);
			expect(MathModule.getStartCoord(outerRadius,startAngle).y).toBeCloseTo(0);

			startAngle = 630;
			outerRadius = 12;
			expect(MathModule.getStartCoord(outerRadius,startAngle).x).toBeCloseTo(0);
			expect(MathModule.getStartCoord(outerRadius,startAngle).y).toBeCloseTo(-12);

		});

		it("Get outer arc coordinates", function() {
			var fullDegrees, outerRadius;

			fullDegrees = 0;
			outerRadius = 140;
			expect(MathModule.getOuterArcCoord(outerRadius,fullDegrees)).toEqual({x:140,y:0});

			fullDegrees = 90;
			outerRadius = 60;
			expect(MathModule.getOuterArcCoord(outerRadius,fullDegrees).x).toBeCloseTo(0);
			expect(MathModule.getOuterArcCoord(outerRadius,fullDegrees).y).toBeCloseTo(60);

			fullDegrees = 180;
			outerRadius = 10;
			expect(MathModule.getOuterArcCoord(outerRadius,fullDegrees).x).toBeCloseTo(-10);
			expect(MathModule.getOuterArcCoord(outerRadius,fullDegrees).y).toBeCloseTo(0);

			fullDegrees = 630;
			outerRadius = 12;
			expect(MathModule.getOuterArcCoord(outerRadius,fullDegrees).x).toBeCloseTo(0);
			expect(MathModule.getOuterArcCoord(outerRadius,fullDegrees).y).toBeCloseTo(-12);

		});

		it("Get inner arc coordinates", function() {
			var fullDegrees, innerRadius;

			fullDegrees = 0;
			innerRadius = 140;
			expect(MathModule.getInnerArcCoord(innerRadius,fullDegrees)).toEqual({x:140,y:0});

			fullDegrees = 90;
			innerRadius = 60;
			expect(MathModule.getInnerArcCoord(innerRadius,fullDegrees).x).toBeCloseTo(0);
			expect(MathModule.getInnerArcCoord(innerRadius,fullDegrees).y).toBeCloseTo(60);

			fullDegrees = 180;
			innerRadius = 10;
			expect(MathModule.getInnerArcCoord(innerRadius,fullDegrees).x).toBeCloseTo(-10);
			expect(MathModule.getInnerArcCoord(innerRadius,fullDegrees).y).toBeCloseTo(0);

			fullDegrees = 630;
			innerRadius = 12;
			expect(MathModule.getInnerArcCoord(innerRadius,fullDegrees).x).toBeCloseTo(0);
			expect(MathModule.getInnerArcCoord(innerRadius,fullDegrees).y).toBeCloseTo(-12);

		});

		it("Get arc flags", function() {
			var degree;

			degree = 160;
			expect(MathModule.getArcFlags(degree)).toEqual({arc1:"0 0 1",arc2:"0 0 0"});

			degree = 225;
			expect(MathModule.getArcFlags(degree)).toEqual({arc1:"0 1 1",arc2:"0 1 0"});

			degree = 480;
			expect(MathModule.getArcFlags(degree)).toEqual({arc1:"0 0 1",arc2:"0 0 0"});

		});

		it("Get line coordinates", function() {
			var fullDegrees, outerRadius, innerRadius, outerArcCoord;

			fullDegrees = 0;
			outerRadius = 140;
			innerRadius = 75;
			outerArcCoord = MathModule.getOuterArcCoord(outerRadius,fullDegrees);
			expect(MathModule.getLineCoord(outerArcCoord.x, outerArcCoord.y, innerRadius)).toEqual({x:75,y:0});

			fullDegrees = 90;
			outerRadius = 60;
			outerArcCoord = MathModule.getOuterArcCoord(outerRadius,fullDegrees);
			innerRadius = 55;
			expect(MathModule.getLineCoord(outerArcCoord.x, outerArcCoord.y, innerRadius).x).toBeCloseTo(0);
			expect(MathModule.getLineCoord(outerArcCoord.x, outerArcCoord.y, innerRadius).y).toBeCloseTo(55);

			fullDegrees = 180;
			outerRadius = 10;
			outerArcCoord = MathModule.getOuterArcCoord(outerRadius,fullDegrees);
			innerRadius = 5;
			expect(MathModule.getLineCoord(outerArcCoord.x, outerArcCoord.y, innerRadius).x).toBeCloseTo(-5);
			expect(MathModule.getLineCoord(outerArcCoord.x, outerArcCoord.y, innerRadius).y).toBeCloseTo(0);

			fullDegrees = 630;
			outerRadius = 12;
			outerArcCoord = MathModule.getOuterArcCoord(outerRadius,fullDegrees);
			innerRadius = 8;
			expect(MathModule.getLineCoord(outerArcCoord.x, outerArcCoord.y, innerRadius).x).toBeCloseTo(0);
			expect(MathModule.getLineCoord(outerArcCoord.x, outerArcCoord.y, innerRadius).y).toBeCloseTo(-8);

		});

	});
});