module.exports = function(grunt) {

	grunt.initConfig({
		concat: {
			options: {
				separator: ';'
			},
			dist: {
				src: ['src/js/math_module.js', 'src/js/draw_module.js', 'src/js/donut_module.js'],
				dest: 'dist/lxdonutchart.js'
			}
		},
		jasmine : {
			// Your project's source files
			src : ['src/js/math_module.js', 'src/js/draw_module.js', 'src/js/donut_module.js'],
			options : {
				specs : 'spec/**/*[sS]pec.js',
				helpers : 'spec/helpers/*.js',
				vendor: ['node_modules/underscore/underscore-min.js', 'src/js/src/**/*.js']
			}
		},
		jshint: {
			all: ['Gruntfile.js', 'src/js/**/*.js', 'spec/**/*.js']
		},
		uglify: {
			my_target: {
				files: {
					'dist/lxdonutchart.min.js': ['src/js/math_module.js', 'src/js/draw_module.js', 'src/js/donut_module.js']
				}
			}
		},
		watch: {
			tests: {
				files: ['src/js/**/*.js', 'spec/**/*[sS]pec.js'],
				tasks: ['jasmine']
			}
		},
		jsdoc : {
			dist : {
				src: ['src/js/**/*.js'],
				options: {
					destination: 'doc',
					template : "node_modules/grunt-jsdoc/node_modules/ink-docstrap/template",
					configure : "node_modules/grunt-jsdoc/node_modules/ink-docstrap/template/jsdoc.conf.json"
				}
			}
		}
	});

	// Register tasks.
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-jasmine');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-jsdoc');

	// Default task.
	grunt.registerTask('default', 'jasmine');
	grunt.registerTask('build', ['concat','uglify']);
};